---
layout: post
title: Autumn School on Skyrmionics
categories: [upcoming]
---

#### 2 & 3 November 2020

An autumn school of the DFG priority program SPP2137 (Skyrmionics) will held on 2. & 3. November 2020. The school will take place at ["Kloster Seeon"](https://www.kloster-seeon.de/kloster-seeon). The planned program comprises tutorial lectures on various areas of research on skyrmions in magnetic materials. Further information will be available soon.
