---
layout: post
title: Retreat Meeting
categories: [upcoming]
---
#### 26 & 27 March 2020

A Retreat Meeting of the DFG priority program SPP2137 (Skyrmionics) will be held on 26 & 27 March 2020. The retreat meeting will take place at the Physik-Zentrum in Bad Honnef. It is expected that the program will start around 10am on 26 March and that it will finish after lunch on 27 March. Arrival is possible on the evening of 25 March.  An outline of the anticipated program may be found [here]({{ site.baseurl }}/assets/PrelimProgram-SPP2137-retreat.pdf).

<!--more-->

To encourage an exchange of experience and ideas on a broader scale, all groups interested in the activities of the priority program are invited to participate, regardless if they receive funding through the priority program.

Accommodation will be available at the Physik-Zentrum. For budgetary reasons participants are asked to pay the accommodation on site. The cost for a single room will be 65/night and for a double room 45/night/person. The costs for the room includes breakfast. Costs for food and coffee breaks will be covered through the coordination project of the priority program.

To register please send the [registration form]({{ site.baseurl }}/assets/Registration-SPP2137-retreat.docx) with the relevant information (name, dates of attendance, title of contribution, etc) at your earliest convenience, but not later than 2 March 2020, to: Lisa Seitz (lisa.seitz@ph.tum.de) at the Technical University of Munich.
