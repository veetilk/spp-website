---
layout: post
title: International Conference
categories: [upcoming]
---

#### 4 - 6 November 2020

An International Conference on Skyrmionics organized by the DFG priority program SPP2137 will held between 4. & 6. November 2020. The Conference will take place at ["Kloster Seeon"](https://www.kloster-seeon.de/kloster-seeon). The planned program comprises invited and contributed talks as well as a poster session on recent scientific progress in the field. Further information will be available soon.
