---
layout: post
title: Network Meeting 11. July 2017
categories: [recent]
---

A network meeting will be organized at the University of Mainz on 11 July 2017. The
main objectives of this meeting are to provide:
* technical information on the administrational procedures of DFG,
* information on the scientific scope of the SPP and its history,
* information on the groups working in this field with the aim to promote an exchange of ideas and discussions on possible joint proposals.

<!--more-->

For organizational reasons groups planning to attend the network meeting are
requested to send a short e-mail at their earliest convenience to the organizer of the
meeting ([Karin Everschor-Sitte](mailto:kaeversc@uni-mainz.de)) or the coordinator of the SPP ([Christian Pfleiderer](mailto:christian.pfleiderer@frm2.tum.de)),
indicating also whether they intend to present a poster.

For further details on the location and program please visit the workshop homepage at [https://www.spice.uni-mainz.de/spp-skyrmions-nwmeeting/](https://www.spice.uni-mainz.de/spp-skyrmions-nwmeeting/).

To download the program click [here]({{ site.baseurl }}/assets/20170602_SPP2137_Vernetzungstreffen.pdf)
