---
layout: post
title: Presentations at the Networking Meeting
categories: [recent]
---

The presentations of the network meeting in Mainz on 11th of July, 2017, are now available for download.
 * [Presentation of C. Schuster (DFG)]({{ site.baseurl }}/assets/20170726_SPP2137_Vernetzungstreffen_aktuell.pdf)
 * [Presentation of C. Pfleiderer (TU München)]({{ site.baseurl }}/assets/20170726_SPP2137_Pfleiderer.pdf)
 * [List of Potential Participants]({{ site.baseurl }}/assets/20170726_SPP2137_potential_participants.pdf)
